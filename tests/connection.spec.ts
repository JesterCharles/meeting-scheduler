import { client } from "../src/connection";

test("Should connect to our sql database", async () => {
	await client.query("select * from meeting");
});

afterAll(async () => {
	await client.end();
});
