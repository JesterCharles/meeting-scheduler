import express from "express";
import cors from "cors";
import { MeetingService } from "./services/meeting-service";
import MeetingServiceImpl from "./services/meeting-service-impl";
import { Meeting } from "./entities";
const middleware = require("./middleware");

const app = express();
app.use(express.json());
app.use(cors());
const meetingService: MeetingService = new MeetingServiceImpl();

// Retrieve all meetings
app.get("/meetings", async (req, res) => {
	try {
		const meetings: Meeting[] = await meetingService.allMeetings();
		res.status(200);
		res.send(meetings);
	} catch (error) {
		res.status(404);
		res.send(error);
	}
});

// Retrieve meeting by ID
app.get("/meetings/:id", async (req, res) => {
	try {
		const meeting: Meeting = await meetingService.meetingByID(Number(req.params.id));
		res.send(meeting);
	} catch (error) {
		res.status(404);
		res.send(error);
	}
});

// Post new meeting to database
app.post("/meetings", middleware, async (req, res) => {
	let meeting: Meeting = req.body;
	meeting = await meetingService.scheduleMeeting(meeting);
	res.status(201);
	res.send(meeting);
});

// Update the meeting
app.put("/meetings/:id", middleware, async (req, res) => {
	try {
		let meeting: Meeting = req.body;
		const meetingId: number = Number(req.params.id);
		meeting.meetingId = meetingId;
		meeting = await meetingService.changeMeeting(meeting);
		res.send(meeting);
	} catch (error) {
		res.status(404);
		res.send(error);
	}
});

// Delete meeting
app.delete("/meetings/:id", middleware, async (req, res) => {
	try {
		const bool: Boolean = await meetingService.cancelMeeting(Number(req.params.id));
		res.status(205);
		res.send(bool);
	} catch (error) {
		res.status(404);
		res.send(error);
	}
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Meeting Scheduler Has Started"));
