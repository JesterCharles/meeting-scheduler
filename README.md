# meeting-scheduler

Here we create the SQL backend for our town meeting scheduler

## If you wish to deploy this project to kubernetes submit the following commands **IN ORDER**

```bash
# SKIP IF you've already run this helm command and yaml, if done within your project no need to repeat
helm install ingress ingress-nginx/ingress-nginx
kubectl apply -f ingress.yaml

# Everything else to build this particular service
kubectl apply -f ms-secret.yaml # Make sure to set this up using your own password for your SQL instance
kubectl apply -f ms-deployment.yaml
kubectl apply -f ms-service.yaml

```
